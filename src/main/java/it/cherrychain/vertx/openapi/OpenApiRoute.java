package it.cherrychain.vertx.openapi;

import io.vertx.ext.web.Router;
import it.cherrychain.vertx.openapi.spec.OpenApi;

import static it.cherrychain.vertx.openapi.spec.OpenApi.openApiSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.infoSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.pathsSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.serversSpec;
import static it.cherrychain.vertx.openapi.spec.OpenApiProperty.tagsSpec;
import static it.cherrychain.vertx.openapi.spec.PathsProperty.pathItemsSpec;
import static java.util.Objects.requireNonNull;

public interface OpenApiRoute {
  static OpenApiRoute openApiRoute(final Router router) {
    return openApiRoute(
      router,
      openApiSpec(
        infoSpec("OpenApi Test", "OpenApi Test Description"),
        serversSpec(),
        tagsSpec(),
        pathsSpec(
          pathItemsSpec(router)
        )
      )
    );
  }

  static OpenApiRoute openApiRoute(final Router router, final OpenApi openApi) {
    return new OpenApiRouteImpl(
      requireNonNull(router, "Router can't be null."),
      requireNonNull(openApi, "OpenApi can't be null.")
    );
  }

  void on(String path);
}
