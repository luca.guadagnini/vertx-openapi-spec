package it.cherrychain.vertx.openapi.spec;

import static java.lang.String.join;

final class PathSpecImpl implements PathSpec {
  private final String[] splitted;

  PathSpecImpl(final String[] splitted) {
    this.splitted = splitted;
  }

  @Override
  public final String get() {
    final var joined = join("/", splitted);
    return joined.startsWith(":")
      ? joined.substring(1)
      : joined;
  }
}
