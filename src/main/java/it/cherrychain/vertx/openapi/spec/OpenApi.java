package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.OpenAPI;

import java.util.function.Supplier;

import static java.util.Arrays.copyOf;

public interface OpenApi extends Supplier<OpenAPI> {
  static OpenApi openApiSpec(final OpenApiProperty<?>... properties) {
    return new OpenApiSpec(copyOf(properties, properties.length));
  }
}
