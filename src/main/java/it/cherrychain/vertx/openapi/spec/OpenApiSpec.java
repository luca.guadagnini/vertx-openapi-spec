package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.OpenAPI;

final class OpenApiSpec implements OpenApi {
  private final OpenApiProperty<?>[] properties;

  OpenApiSpec(final OpenApiProperty<?>[] properties) {
    this.properties = properties;
  }

  @Override
  public final OpenAPI get() {
    final var openApi = new OpenAPI();
    for (final var property : properties) property.apply(openApi);
    return openApi;
  }
}
