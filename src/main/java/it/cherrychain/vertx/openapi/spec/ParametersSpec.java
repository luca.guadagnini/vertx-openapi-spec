package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.parameters.Parameter;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

final class ParametersSpec implements OperationProperty<List<Parameter>> {
  private final String[] splittedPath;

  ParametersSpec(final String[] splittedPath) {
    this.splittedPath = splittedPath;
  }

  @Override
  public final List<Parameter> apply(final Operation operation) {
    final var parameters = Stream.of(splittedPath)
      .filter(it -> it.startsWith(":"))
      .map(this::asParameter)
      .collect(toList());

    operation.setParameters(parameters);

    return parameters;
  }

  private Parameter asParameter(final String name) {
    final var parameter = new Parameter();
    parameter.setName(name.substring(1));
    parameter.setRequired(true);
    parameter.setAllowEmptyValue(false);
    return parameter;
  }
}
