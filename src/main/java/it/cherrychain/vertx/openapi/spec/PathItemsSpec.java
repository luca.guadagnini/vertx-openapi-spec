package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;
import io.vertx.ext.web.Router;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static it.cherrychain.vertx.openapi.spec.OperationProperty.parametersSpec;
import static it.cherrychain.vertx.openapi.spec.PathItemProperty.operationSpec;
import static it.cherrychain.vertx.openapi.spec.PathSpec.pathSpec;
import static it.cherrychain.vertx.openapi.spec.PathsProperty.pathItemSpec;
import static it.cherrychain.vertx.openapi.spec.RouteMethods.routeMethods;
import static java.util.Objects.nonNull;
import static java.util.stream.Collectors.toList;

final class PathItemsSpec implements PathsProperty<List<PathItem>> {
  private static final Logger log = LoggerFactory.getLogger(PathItemsSpec.class);

  private final Router router;

  PathItemsSpec(final Router router) {
    this.router = router;
  }

  @Override
  public final List<PathItem> apply(final Paths paths) {
    log.info("Router.routes: {}", router.getRoutes());
    return router.getRoutes().stream()
      .filter(it -> nonNull(it.getPath()))
      .map(it ->
        pathItemSpec(
          pathSpec(it.getPath()),
          operationSpec(
            routeMethods(it),
            parametersSpec(it.getPath())
          )
        )
      )
      .map(it -> it.apply(paths))
      .collect(toList());
  }
}
