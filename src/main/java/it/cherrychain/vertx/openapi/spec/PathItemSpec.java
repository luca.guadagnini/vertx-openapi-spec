package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.Paths;

final class PathItemSpec implements PathsProperty<PathItem> {
  private final PathSpec pathSpec;
  private final PathItemProperty<?>[] properties;

  PathItemSpec(final PathSpec pathSpec, final PathItemProperty<?>[] properties) {
    this.pathSpec = pathSpec;
    this.properties = properties;
  }

  @Override
  public final PathItem apply(final Paths paths) {
    final var pathItem = new PathItem();
    for (final var property : properties) property.apply(pathItem);
    paths.put(pathSpec.get(), pathItem);
    return pathItem;
  }
}
