package it.cherrychain.vertx.openapi.spec;


import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.parameters.Parameter;

import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.copyOf;
import static java.util.Objects.requireNonNull;

public interface OperationProperty<T> extends Function<Operation, T> {
  private static OperationProperty<List<Parameter>> parametersSpec(final String... splitted) {
    return new ParametersSpec(copyOf(splitted, splitted.length));
  }

  static OperationProperty<List<Parameter>> parametersSpec(final String path) {
    return parametersSpec(requireNonNull(path, "Path can't be null.").split("/"));
  }
}
