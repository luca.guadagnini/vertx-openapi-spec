package it.cherrychain.vertx.openapi.spec;

import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.Route;

import static java.util.Objects.requireNonNull;

public interface RouteMethods extends Iterable<HttpMethod> {
  static RouteMethods routeMethods(final Route route) {
    return new RouteMethodsImpl(requireNonNull(route, "Route can't be null."));
  }
}
