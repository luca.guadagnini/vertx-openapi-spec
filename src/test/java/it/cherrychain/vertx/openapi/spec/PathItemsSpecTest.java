package it.cherrychain.vertx.openapi.spec;

import io.swagger.v3.oas.models.Paths;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import org.junit.jupiter.api.Test;

import static io.vertx.core.Vertx.vertx;
import static io.vertx.ext.web.Router.router;
import static it.cherrychain.vertx.openapi.spec.PathsProperty.pathItemsSpec;
import static org.assertj.core.api.Assertions.assertThat;

class PathItemsSpecTest {
  private final Router router = router(vertx());
  private final Route route = router.get("/api/:any/resource/:id").handler(it -> it.response().end());

  @Test
  void shouldHavePaths() {
    final var pathItems = pathItemsSpec(router);

    assertThat(pathItems.apply(new Paths())).hasSize(1);
  }
}