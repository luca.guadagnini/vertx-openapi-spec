package it.cherrychain.vertx.openapi;

import io.vertx.core.http.HttpServer;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;
import static io.vertx.core.Vertx.vertx;
import static io.vertx.ext.web.Router.router;
import static it.cherrychain.vertx.openapi.OpenApiRoute.openApiRoute;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

class OpenApiRouteImplTest {
  @Test
  @DisplayName("Should retrieve OpenApi specs as Json")
  void shouldRetrieveJson() throws ExecutionException, InterruptedException {
    final var vertx = vertx();
    final var router = router(vertx);

    final var route1 = router.get("/api/:any/resource1/:id").handler(it -> it.response().end());
    final var route2 = router.get("/api/:some/resource2/:uuid").handler(it -> it.response().end());

    openApiRoute(router).on("/openapi");

    final var promise = new CompletableFuture<HttpServer>();
    vertx.createHttpServer().requestHandler(router).listen(1234, async -> promise.complete(async.result()));
    final var server = promise.get();

    assertThat(server).isNotNull();

    given()
      .port(1234)
      .contentType(JSON)
      .get("/openapi")
    .then()
      .contentType(JSON)
      .statusCode(200)
      .body("openapi", equalTo("3.0.1"));

    vertx.close();
  }

  @Test
  @DisplayName("Should retrieve OpenApi specs as Yaml")
  void shouldRetrieveYaml() throws ExecutionException, InterruptedException {
    final var vertx = vertx();
    final var router = router(vertx);

    final var route1 = router.get("/api/:any/resource1/:id").handler(it -> it.response().end());
    final var route2 = router.get("/api/:some/resource2/:uuid").handler(it -> it.response().end());

    openApiRoute(router).on("/openapi");

    final var promise = new CompletableFuture<HttpServer>();
    vertx.createHttpServer().requestHandler(router).listen(1234, async -> promise.complete(async.result()));
    final var server = promise.get();

    assertThat(server).isNotNull();

    given()
      .port(1234)
      .contentType("application/yaml; charset=UTF-8")
      .get("/openapi")
    .then()
      .contentType("application/yaml; charset=UTF-8")
      .statusCode(200)
      .body(containsString("3.0.1"));

    vertx.close();
  }
}